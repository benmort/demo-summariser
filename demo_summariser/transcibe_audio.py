# https://github.com/openai/whisper/discussions/918
import torch
import time
import whisper
import os
import json

# 1208.53 s FP16 = True, medium, CUDA
# 950.45 s FP16 = False, medium, CUDA
# 930.04s FP16 = False, medium, CPU
# 1129.94s FP16 = True, medium, CPU

def save_text_to_file(text, file_path):
    try:
        with open(file_path, "w", encoding="utf-8") as file:
            file.write(text)
        print(f"Text saved successfully to {file_path}")
    except Exception as e:
        print(f"Error: {e}")


def save_dict_to_json(dictionary, file_path):
    try:
        with open(file_path, "w", encoding="utf-8") as json_file:
            json.dump(dictionary, json_file, indent=4)
        print(f"Dictionary saved successfully to {file_path}")
    except Exception as e:
        print(f"Error: {e}")


fp16 = False
model_name = "medium"
# device = torch.device("cuda")
# model = whisper.load_model(model_name, device=device, download_root=".")
model = whisper.load_model(model_name, download_root=".")

duration = 0
start_time = time.time()
result = model.transcribe(
    "GMT20231123-075535_Recording.m4a",
    language="en",
    task="transcribe",
    fp16=fp16,
    word_timestamps=True,
)
end_time = time.time()
duration = end_time - start_time
print(json.dumps(result, indent=2))

print("{} model with fp16 {} costs {:.2f}s".format(model_name, fp16, duration))

file_path = "demo_text.txt"
save_text_to_file(result["text"], file_path)

file_path = "demo.json"
save_dict_to_json(result, file_path)
