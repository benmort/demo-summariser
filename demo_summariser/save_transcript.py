import json


def load_json_file(file_path):
    """
    Load a JSON file into a dictionary.

    Parameters:
    - file_path (str): The path to the JSON file.

    Returns:
    - dict: The dictionary containing the JSON data.
    """
    with open(file_path, "r") as file:
        data = json.load(file)
    return data


file_path = "demo.json"
my_dict = load_json_file(file_path)

transcript = []
for segment in my_dict["segments"]:
    # print(f'{segment["id"]:04d}: {segment["start"]:10.2f} : {segment["text"]}')
    # if segment["id"] > 5:
    #     break
    transcript.append((segment["id"], segment["start"], segment["text"]))


file_path = "transcript.txt"
with open(file_path, "w") as file:
    for line in transcript:
        file.write(f"{line[0]:04d} {line[1]:10.2f} {line[2]}" + "\n")
