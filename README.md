# Project to experiment with transcription and summarising demos

Uses vector [embeddings](https://platform.openai.com/docs/guides/embeddings) 
stored in [pinecone](https://www.pinecone.io/) in an attempt to reduce API 
calls to the OpenAI LLM.

## Files

* `example.ipynb`: Notebook exploring how to use the OpenAI LLM with vector
  embeddings stored in pinecone to query demo transcripts.
* `save_transcript.py`: Utility function to converts JSON output from 
   whisper to a demo transcript text file.
* `transcribe_audio.py`: Simple function to transcribe audio from a zoom demo
   recording into a transcript.